# auth0-vue-plugin

Plugin for Auth0 integration with a Vue application. This is based on the Auth0 VueJS integration sample with some added functionality

## Usage
Import the package before initializing you Vue app

    import Auth from '@mrzwick/auth0-vue-plugin';

Instantiate your domain and clientId values provided by Auth0.

    const domain = process.env.VUE_APP_AUTH_DOMAIN;
    const clientId = process.env.VUE_APP_AUTHCLIENT_ID;

Setup the Vue object to import the plugin.

    Vue.use(Auth.Auth0Plugin, {
      domain,
      clientId,
    });

Use the $auth instance within your Vue components to access authentication methods and data

Using Auth within a component template

    <button v-if="!$auth.isAuthenticated" @click="login">Log in</button>
    <button v-if="$auth.isAuthenticated" @click="logout">Log out</button>

Using auth within a component script

    export default {
      name: 'Home',

      methods: {
        // Log the user in
        login() {
          this.$auth.loginWithPopup();
        },
        // Log the user out
        logout() {
          this.$auth.logout({
            returnTo: window.location.origin,
          });
        },
      },
    };