// Login a user
export const LOGIN = 'USER_LOGIN';
export const LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const LOGIN_FAILED = 'USER_LOGIN_FAILURE';
export const LOGOUT = 'USER_LOGOUT';