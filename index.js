import * as AuthMutations from './src/store/mutation_types';
import { Auth0Plugin } from './src/lib/authenticate';

export default { AuthMutations, Auth0Plugin };